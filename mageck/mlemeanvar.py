'''
Modeling mean and variance
'''

from __future__ import print_function

import re
import sys
import scipy
from scipy.stats import nbinom
from scipy.stats import linregress
import random
import math
import numpy as np
import numpy.linalg as linalg
import logging


# debug
try:
  from IPython.core.debugger import Tracer
except:
  pass

from mleclassdef import *
from mlelowess import *

class MeanVarModel:
  '''
  The Mean and Variance model class
  '''
  # variables for storing k (read counts) and residule
  list_k=None
  list_res=None
  # for lowess regression
  list_lowess=None
  lowess_search={} # used to identify the lowess regression values of log(variance+0.01)
  # for linear regression
  lm_intercept=0.0
  lm_coeff=0.0
  # member functions
  def get_mean_var_residule(self,allgenedict):
    '''
    Getting the read counts and regression residules for each sgRNA
    part of the modeling the mean and variance
    '''
    list_k=[]
    list_res=[]
    for (gid,gsk) in allgenedict.iteritems():
      nsg=len(gsk.nb_count[0])
      nsample=len(gsk.nb_count)
      if len(gsk.sgrna_kvalue)>0:
        sg_k=[x[0] for x in gsk.sgrna_kvalue.tolist()]
        sg_residule=[x[0] for x in gsk.sgrna_residule.tolist()]
        if len(sg_k)>=nsg*nsample:
          list_k+=sg_k[:(nsg*nsample)]
          list_res+=sg_residule[:(nsg*nsample)]
    np_k=np.array(list_k)
    np_res=np.array(list_res)
    self.list_k=np_k
    self.list_res=np_res
  #
  def save_k_residule_to_file(self,filename):
    # save the k and residule to file
    fileoh=open(filename,'w')
    for i in range(len(self.list_k)):
      print('\t'.join([str(self.list_k[i]),str(self.list_res[i])]),file=fileoh)
    fileoh.close()
  def model_mean_var_by_lowess(self):
    '''
    Modeling the mean and variance by LOWESS regression
    Too slow, need some changes in the future
    '''
    k_log=np.log(self.list_k)
    var_log=np.log(np.square(self.list_res)+0.01)
    self.list_lowess=lowess(k_log,var_log)
    # save to a dictionary
    for i in range(len(self.list_k)):
      self.lowess_search[self.list_k[i]]=self.list_lowess[i]
  #
  def get_lowess_var(self,klist,returnalpha=False):
    '''
    Return the fitted values of variance.
    If returnalpha=True, return the alpha value (var=mean+alpha*mean^2)
    '''
    if self.list_lowess==None:
      raise ValueError('Lowess regression has not been done yet.')
    for k in klist:
      if k not in self.lowess_search:
        raise ValueError('the key value is not in the dictionary.')
    varvalue=np.array([self.lowess_search[k] for k in klist])
    varvalue=np.exp(varvalue)-0.01
    varvalue=np.array([ (lambda x: x if x>=0.01 else 0.01)(t) for t in varvalue])
    # 
    if returnalpha:
      #alphavalue=(varvalue-k)/(k**2)
      alphavalue=[(varvalue[i]-klist[i])/(klist[i]**2) for i in range(len(klist))]
      alphavalue=np.array([ (lambda x: x if x>=0.01 else 0.01)(t) for t in alphavalue])
      return alphavalue
    else:
      return varvalue
  def model_mean_var_by_lm(self):
    '''
    Modeling the mean and variance by linear regression
    '''
    k_log=np.log(self.list_k)
    var_log=np.log(np.square(self.list_res)+0.01)
    # remove those with too low variance
    k_log2=np.array([k_log[i] for i in range(len(var_log)) if var_log[i]>(-1)])
    var_log2=np.array([var_log[i] for i in range(len(var_log)) if var_log[i]>(-1)])
    (slope,intercept,r_value,p_value,std_err)=linregress(k_log2,var_log2)
    # Tracer()()
    self.lm_intercept=intercept
    self.lm_coeff=slope
    logging.info('Linear regression: y='+str(slope)+'x+'+str(intercept))
  def get_lm_var(self,klist,returnalpha=False):
    '''
    Return the fitted values of variance.
    If returnalpha=True, return the alpha value (var=mean+alpha*mean^2)
    '''
    kls=(klist)
    k_log=np.log(kls)
    varvalue=k_log*self.lm_coeff+self.lm_intercept
    varvalue=np.exp(varvalue)-0.01
    #varvalue=np.array([ (lambda x: x if x>=0.01 else 0.01)(t) for t in varvalue])
    varvalue=np.where(varvalue>0.01,varvalue,0.01)
    # set up the lower bound
    th_count=10.0
    var_t=np.log(th_count)*self.lm_coeff+self.lm_intercept
    var_t=np.exp(var_t)-0.01
    if var_t<0.01:
      var_t=0.01
    alpha_t=(var_t-th_count)/(th_count*th_count)
    if alpha_t<1e-2:
      alpha_t=1e-2
    # 
    if returnalpha:
      #alphavalue=[(varvalue[i]-kls[i])/(kls[i]**2) for i in range(len(kls))]
      #alphavalue=np.array([ (lambda x: x if x>=0.01 else 0.01)(t) for t in alphavalue])
      alphavalue=np.divide((varvalue-kls),np.multiply(kls,kls))
      alphavalue2=np.where(alphavalue>alpha_t,alphavalue,alpha_t)
      return alphavalue2
    else:
      return varvalue
   




