#!/usr/bin/env python
'''
MAGeCK MLE main entry
'''

from __future__ import print_function

import re
import sys
import random
import math
import logging

# debug
try:
  from IPython.core.debugger import Tracer
except:
  pass

# importing predefined functions
from mleargparse import *


def mageckmle_main(pvargs=None,parsedargs=None,returndict=False):
  '''
  Main entry for MAGeCK MLE
  ----
  Parameters:

  pvargs
    Arguments for parsing
  returndict
    If set true, will not try to run the whole prediction process, but will return after mean variance modeling
  '''
  # parsing arguments
  if parsedargs is not None:
    args=parsedargs
  else:
    args=mageckmle_parseargs(pvargs)
  args=mageckmle_postargs(args)
  # from mleclassdef import *
  # from mledesignmat import *
  # from mleem import *
  # from mleinstanceio import *
  # from mlemeanvar import *
  import scipy
  from scipy.stats import nbinom
  import numpy as np
  import numpy.linalg as linalg
  from mleinstanceio import read_gene_from_file,write_gene_to_file,write_sgrna_to_file
  from mleem import iteratenbem,iteratenbem_permutation
  from mlemeanvar import MeanVarModel
  # main process
  maxfittinggene=args.genes_varmodeling
  maxgene=np.inf
  # reading the file
  allgenedict=read_gene_from_file(args.count_table,includesamples=args.include_samples)
  # desmat=np.matrix([[1,1,1,1],[0,0,1,0],[0,0,0,1]]).getT()
  desmat=args.design_matrix
  ngene=0
  for (tgid,tginst) in allgenedict.iteritems():
    tginst.design_mat=desmat
  for (tgid,tginst) in allgenedict.iteritems():
    if ngene % 1000 ==1:
      logging.info('Calculating '+tgid+' ('+str(ngene)+') ... ')
    iteratenbem(tginst,debug=False,estimateeff=True)
    ngene+=1
    tginst.w_estimate=[]
    if ngene>maxfittinggene:
      break
  # model the mean and variance
  # write_gene_to_file(allgenedict,'results/tim/tim.meanvar_initial.gene.txt')
  # write_sgrna_to_file(allgenedict,'results/tim/tim.meanvar_initial.sgrna.txt')
  logging.info('Modeling the mean and variance ...')
  if maxfittinggene>0:
    mrm=MeanVarModel()
    mrm.get_mean_var_residule(allgenedict)
    mrm.model_mean_var_by_lm()
  else:
    mrm=None
  # mrm.save_k_residule_to_file('results/tim/tim.meanvar.model.txt')
  
  if returndict:
    return (allgenedict,mrm)
  # run the test again...
  logging.info('Run the algorithm for the second time ...')
  ngene=0
  for (tgid,tginst) in allgenedict.iteritems():
    #try:
    if ngene % 1000 ==1:
      logging.info('Calculating '+tgid+' ('+str(ngene)+') ... ')
    tginst.design_mat=desmat
    # Tracer()()
    iteratenbem(tginst,debug=False,estimateeff=True,meanvarmodel=mrm,restart=False,removeoutliers=args.remove_outliers)
    ngene+=1
    if ngene>maxgene:
      break
    #except:
    #  logging.error('Error occurs while calculating beta values of gene '+tgid+'.')
    #  sys.exit(-1)
  # permutation
  iteratenbem_permutation(allgenedict,nround=args.permutation_round,removeoutliers=args.remove_outliers)
  # correct for FDR
  from mleclassdef import gene_fdr_correction;
  gene_fdr_correction(allgenedict,args.adjust_method);
  # write to file
  genefile=args.output_prefix+'.gene_summary.txt'
  sgrnafile=args.output_prefix+'.sgrna_summary.txt'
  logging.info('Writing gene results to '+genefile)
  logging.info('Writing sgRNA results to '+sgrnafile)
  write_gene_to_file(allgenedict,genefile,betalabels=args.beta_labels)
  write_sgrna_to_file(allgenedict,sgrnafile)
  return (allgenedict,mrm)


if __name__ == '__main__':
  try:
    mageckmle_main()
  except KeyboardInterrupt:
    sys.stderr.write("User interrupt me! ;-) Bye!\n")
    sys.exit(0)


